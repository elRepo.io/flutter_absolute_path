package net.altermundi.flutter_absolute_path;

import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

//To get activity and context
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import android.app.Activity;
import android.content.Context;
import androidx.annotation.NonNull;

//Import pickit
import com.hbisoft.pickit.PickiT;
import com.hbisoft.pickit.PickiTCallbacks;

import java.util.ArrayList;
import java.util.Objects;
import android.annotation.SuppressLint;
import android.os.Build;

// For absolutepath
import android.net.Uri;


/** FlutterAbsolutePathPlugin */
public class FlutterAbsolutePathPlugin implements FlutterPlugin, MethodCallHandler, ActivityAware, PickiTCallbacks {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;

  private Context context;
  private Activity activity;

  //Declare PickiT
  private PickiT pickiT;

  private Result _result;


  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "flutter_absolute_path");
    channel.setMethodCallHandler(this);

    context = flutterPluginBinding.getApplicationContext();

    //Initialize PickiT
    //context, listener, activity
    pickiT = new PickiT(context, this, activity);
  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
    if (call.method.equals("getAbsolutePath")) {
      String uriString = (String) call.argument("uri");
      Uri uri = Uri.parse(uriString);
      if(pickiT.isUnknownProvider(uri, Build.VERSION.SDK_INT)  || !pickiT.wasLocalFileSelected(uri)) {
        result.error("IS_REMOTE_FILE", "The selected file is remote and is not implemented yet", null);
      }
      this._result = result;
      pickiT.getPath(uri, android.os.Build.VERSION.SDK_INT);
    } else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
  }



  //  To get context and activity
  @Override
  public void onDetachedFromActivity() {
  }

  @Override
  public void onReattachedToActivityForConfigChanges(ActivityPluginBinding binding) {
  }

  @Override
  public void onAttachedToActivity(ActivityPluginBinding binding) {
    this.activity = binding.getActivity();
  }

  @Override
  public void onDetachedFromActivityForConfigChanges() {
  }


  // https://github.com/HBiSoft/PickiT/blob/master/app/src/main/java/com/hbisoft/pickitexample/MainActivity.java

  //  PickiT Listeners
  //
  //  The listeners can be used to display a Dialog when a file is selected from Dropbox/Google Drive or OnDrive.
  //  The listeners are callbacks from an AsyncTask that creates a new File of the original in /storage/emulated/0/Android/data/your.package.name/files/Temp/
  //
  //  PickiTonUriReturned()
  //  When selecting a file from Google Drive, for example, the Uri will be returned before the file is available(if it has not yet been cached/downloaded).
  //  Google Drive will first have to download the file before we have access to it.
  //  This can be used to let the user know that we(the application), are waiting for the file to be returned.
  //
  //  PickiTonStartListener()
  //  This will be call once the file creations starts and will only be called if the selected file is not local
  //
  //  PickiTonProgressUpdate(int progress)
  //  This will return the progress of the file creation (in percentage) and will only be called if the selected file is not local
  //
  //  PickiTonCompleteListener(String path, boolean wasDriveFile)
  //  If the selected file was from Dropbox/Google Drive or OnDrive, then this will be called after the file was created.
  //  If the selected file was a local file then this will be called directly, returning the path as a String
  //  Additionally, a boolean will be returned letting you know if the file selected was from Dropbox/Google Drive or OnDrive.

  @Override
  public void PickiTonUriReturned() {
  }

  @Override
  public void PickiTonStartListener() {
  }

  @Override
  public void PickiTonProgressUpdate(int progress) {
  }

  @SuppressLint("SetTextI18n")
  @Override
  public void PickiTonCompleteListener(
          String path, boolean wasDriveFile, boolean wasUnknownProvider, boolean wasSuccessful, String reason) {
    if(path == null) this._result.error("PATH_IS_NULL", "Can't retrieve file path", null);
    else this._result.success(path);
  }

  @Override
  public void PickiTonMultipleCompleteListener(ArrayList<String> paths, boolean wasSuccessful, String Reason) {
  }
}
