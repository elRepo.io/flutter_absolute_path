# flutter_absolute_path

Implement flutter absolute path using [PickiT](https://github.com/HBiSoft/PickiT) library.

Android only supported.

For the moment only local files are supported, remote files will be supported soon.

## Add it to your project

Add the following in your root build.gradle at the end of repositories:

```gradle
allprojects {
    repositories {
    ...
    maven { url 'https://jitpack.io' }
    }
}
```

Then, add the dependency, in your app level build.gradle:

```
dependencies {
    implementation 'com.github.HBiSoft:PickiT:2.0.1'
}
```

## Use it

Simply: 

```dart
var filePath = await FlutterAbsolutePath.getAbsolutePath(filePath);
```

Platform codes exception are defined on: `PlatformErrorCodes`.

```dart
try {
    final filePath =
      await FlutterAbsolutePath.getAbsolutePath(asset.identifier ?? '');
    files.add(File(filePath));
} on PlatformException catch (e) {
    if (e.code == PlatformErrorCodes.IS_REMOTE_FILE) files.add("Remote file");
    else if (e.code == PlatformErrorCodes.PATH_IS_NULL) files.add("Path is null");
    else rethrow;
}
```
